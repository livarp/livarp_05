# livarp makefile -- arnault perret <arpinux@member.fsf.org>

all: 
	@echo "Usage: "
	@echo "make 586       : build livarp i386 586"
	@echo "make 686       : build livarp i386 686-pae"
	@echo "make 64        : build livarp amd64"
	@echo "make clean     : clean up build directories"
	@echo "make cleanfull : clean up build & cache directories"

586: clean
	@echo "-------------------"
	@echo "building livarp 586"
	@echo "-------------------"
	cp auto/config586 auto/config
	lb build
	rm auto/config

686: clean
	@echo "-----------------------"
	@echo "building livarp 686-pae"
	@echo "-----------------------"
	cp auto/config686 auto/config
	lb build
	rm auto/config

64: clean
	@echo "---------------------"
	@echo "building livarp amd64"
	@echo "---------------------"
	cp auto/config64 auto/config
	lb build
	rm auto/config

clean:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	lb clean

cleanfull: clean
	@echo "----------------------------------"
	@echo "cleaning build & cache directories"
	@echo "----------------------------------"
	rm -R -f cache
	rm -f ./*.log

